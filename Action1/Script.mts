﻿'   users:
'      USERS_1:ODCZYT|READ
'      USERS_2:ODCZYT|MODIFY
'      USERS_3:ODCZYT|DELETE
'      USERS_4:ODCZYT|OPEN
'   arguments:
'      Radom
'      parameter2
'   libraries:  
'      Standard
'

Function Main

'---------->Parametry globalne uslugi
Dim G_WEJSCIE
Dim G_WYNIK_SKRYPTU_USLUGOWEGO,G_WYNIK_SKRYPTU_KOMPONENTOWEGO,G_WYNIK_SKRYPTU_ITERACYJNEGO
Dim G_SCIEZKA_PLIKU_Z_PARAMETRAMI,G_LICZBA_ITERACJI,G_LICZNIK, G_UZYTKOWNIK
Dim G_PARAMETRY_ENGINE

G_LICZNIK = 0

'--------->Odczyt wymaganych parametrów wejściowych
G_PARAMETRY_ENGINE = Parameter("WEJSCIE")
G_SCIEZKA_PLIKU_Z_PARAMETRAMI = OdczytajParametr(G_PARAMETRY_ENGINE,"SCIEZKA_PLIKU_Z_PARAMETRAMI")
WstawParametr G_WEJSCIE,"SCIEZKA_PLIKU_Z_PARAMETRAMI",G_SCIEZKA_PLIKU_Z_PARAMETRAMI


'--------->Ustawienie wartosci bazowych zmiennych wyjsciowych
ModyfikujParametr G_WYNIK_SKRYPTU_USLUGOWEGO,"KOMUNIKAT_KONCOWY","BRAK"
ModyfikujParametr G_WYNIK_SKRYPTU_USLUGOWEGO,"STATUS","PASSED"
ModyfikujParametr G_WEJSCIE, "G_SCIEZKA", "http://www.google.pl/"
'
'
'--------->Wykonanie kodu uslugi
Do
	Logger.Info G_PARAMETRY_ENGINE
	Exit Do
Loop While True

ModyfikujParametr G_WYNIK_SKRYPTU_USLUGOWEGO,"STATUS","PASSED"
ModyfikujParametr G_WYNIK_SKRYPTU_USLUGOWEGO,"KOMUNIKAT_KONCOWY",G_KOMUNIKAT_KONCOWY
ExitAction(G_WYNIK_SKRYPTU_USLUGOWEGO)
End Function

On Error Resume Next

Main

If LEN(Err.description) <> 0 Then
	RecoveryObj null, Environment("TestName"), err.Description, err.number
End If
